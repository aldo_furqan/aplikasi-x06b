package furqan.albarado.appx06b

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_nilai.*
import kotlinx.android.synthetic.main.frag_data_nilai.view.*

class FragmentNilai : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertNilai->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateNilai->{

            }
            R.id.btnDeleteNilai->{

            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter : SimpleCursorAdapter
    lateinit var spAdapter1 : SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var db : SQLiteDatabase
    var namaMhs : String = ""
    var namaMatkul: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_nilai,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteNilai.setOnClickListener(this)
        v.btnInsertNilai.setOnClickListener(this)
        v.btnUpdateNilai.setOnClickListener(this)
        v.spNama.onItemSelectedListener = this
        v.spMatkul.onItemSelectedListener = this
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataNilaiX()
        showDataMatkul()
        showDataMhs()
    }

    fun showDataNilaiX(){
        var sql= "select n.id_nilai as _id, m.nama, k.nmatkul, n.nilai from nilai n join mhs m on n.nim = m.nim join matkul k on n.kode = k.kode order by n.id_nilai asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_nilai,c,
            arrayOf("_id","nama","nmatkul","nilai"), intArrayOf(R.id.txIdNilai,R.id.txNama,R.id.txMatkul,R.id.txNilai),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsNilai.adapter = lsAdapter
    }

    fun showDataMhs(){
        val c : Cursor = db.rawQuery("select nama as _id from mhs order by nama asc",null)
        spAdapter1 = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spNama.adapter = spAdapter1
        v.spNama.setSelection(0)
    }

    fun showDataMatkul(){
        val c1 : Cursor = db.rawQuery("select nmatkul as _id from matkul order by nmatkul asc",null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c1,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spMatkul.adapter = spAdapter
        v.spMatkul.setSelection(0)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spMatkul.setSelection(0,true)
        spNama.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c = spAdapter1.getItem(position) as Cursor
        val c1 = spAdapter.getItem(position) as Cursor
        namaMatkul = c1.getString(c.getColumnIndex("_id"))
        namaMhs = c.getString(c.getColumnIndex("_id"))
    }

    fun insertDataNilai(nama : String,nmatkul : String, nilai : String){
            var sql = "insert into nilai(nim, kode, nilai) values (?,?,?)"
            db.execSQL(sql, arrayOf(nama,nmatkul,nilai))
            showDataNilaiX()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select nim from mhs where nama='$namaMhs'"
        var sql1 = "select kode from matkul where nmatkul='$namaMatkul'"
        var c : Cursor = db.rawQuery(sql,null)
        var c1 : Cursor = db.rawQuery(sql1,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataNilai(c.getString(c.getColumnIndex("nim")),c1.getString(c1.getColumnIndex("kode")),v.edNilai.text.toString())
            v.edNilai.setText("")

        }
        var x = v.edNilai.text.toString()
        v.txVnilai.setText("$namaMhs, $namaMatkul,$x")
        v.edNilai.setText("")
    }
}