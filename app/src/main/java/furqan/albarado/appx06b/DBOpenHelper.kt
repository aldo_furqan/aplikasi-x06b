package furqan.albarado.appx06b

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tMatkul = "create table matkul(kode text primary key, nmatkul text not null, id_prodi int not null)"
        val tMhs = "create table mhs(nim text primary key, nama text not null, id_prodi int not null)"
        val tNilai = "create table nilai(id_nilai integer primary key autoincrement, nim text not null, kode text not null, nilai text not null)"
        val tProdi = "create table prodi(id_prodi integer primary key autoincrement, nama_prodi text not null)"
        val insProdi = "insert into prodi(nama_prodi) values('AK'),('ME'),('MI')"
        db?.execSQL(tMatkul)
        db?.execSQL(tNilai)
        db?.execSQL(tMhs)
        db?.execSQL(tProdi)
        db?.execSQL(insProdi)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object {
        val DB_Name = "mahasiswa"
        val DB_Ver = 1
    }
}